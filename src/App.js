import Home from "./components/home";
import Procesando from "./components/procesando";
import ValidacionError from "./components/validacionError";
import { Route, Routes } from "react-router-dom";
import "./App.css";

function App() {
  return (
    <main className="container">
      <Routes>
        <Route path="/procesando" element={<Procesando />} />
        <Route path="/validacionError" element={<ValidacionError />} />
        <Route path="/" element={<Home />} />
      </Routes>
    </main>
  );
}

export default App;
