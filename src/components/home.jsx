import React, { useEffect, useState } from "react";
import c from "../config/constantes";
import BarcodeScanner from "./barCodeScanner";
import restService from "../services/restService";

var intervalConsOperatividad;

export default function Home() {
  //const [estadoKiosko, setEstadoKiosko] = useState("");
  const [operatividadKiosko, setOperatividadKiosko] = useState("");

  useEffect(() => {
    async function fEstadoKiosko() {
      const dataEstadoKiosko = await restService.post(
        c.apiEndpoint + c.apiSetearEstadoKiosko,
        { estado: "1", trace: "000000" }
      );
      //setEstadoKiosko(dataEstadoKiosko.data.estado);
    }

    async function fOperatividadKiosko() {
      const dataOperatividadKiosko = await restService.post(
        c.apiEndpoint + c.apiOperatividadKiosko,
        { trace: "000000" }
      );
      setOperatividadKiosko(dataOperatividadKiosko.data.estado);
    }

    fEstadoKiosko().then(
      (intervalConsOperatividad = setInterval(() => {
        fOperatividadKiosko();
      }, 4000))
    );

    return () => clearInterval(intervalConsOperatividad);
  }, []);

  return operatividadKiosko === "0" ? (
    <div>
      <h3>BIENVENIDO A LA CLÍNICA INTERNACIONAL </h3>
      <p>
        <span>
          Por favor, acerca el código de barras de tu ticket al lector ubicado
          en la parte inferior derecha
        </span>
      </p>
      <h1>IMAGEN</h1>
      <BarcodeScanner />
      <a href="/procesando">ticket</a>
    </div>
  ) : (
    <div>
      <h3>TERMINAL INOPERATIVO. Espere a que el kiosko este disponible...</h3>
    </div>
  );
}
