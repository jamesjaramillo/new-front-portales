import React, { useState } from "react";

function BarcodeScanner() {
  const [barcodeInputValue, updateBarcodeInputValue] = useState("");

  function barcodeAutoFocus() {
    document.getElementById("SearchbyScanning").focus();
  }

  function onChangeBarcode(event) {
    updateBarcodeInputValue(event.target.value);
  }

  function onKeyPressBarcode(event) {
    if (event.keyCode === 13) {
      console.log("Barcode: " + barcodeInputValue);
      updateBarcodeInputValue(event.target.value);
    }
  }

  return (
    <div>
      <input
        id="SearchbyScanning"
        hidden={true}
        autoFocus
        value={barcodeInputValue}
        onChange={onChangeBarcode}
        className="SearchInput"
        onKeyDown={onKeyPressBarcode}
        onBlur={barcodeAutoFocus}
      />
    </div>
  );
}

export default BarcodeScanner;
