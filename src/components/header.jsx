import React from "react";

const Header = () => {
  return (
    <div className="container">
      <nav className="navbar navbar-expand-lg navbar-light bg-light">
        <h1 className="navbar-brand mb-0 h1"> HEADER PORTALES </h1>
      </nav>
    </div>
  );
};

export default Header;
