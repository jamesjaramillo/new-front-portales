import React from "react";

const Procesando = () => {
  return (
    <React.Fragment>
      <h3>Estamos procesando tu solicitud.</h3>
      <h3>Por favor, espera unos segundos</h3>
      <span>GIF DE ESPERA</span>
    </React.Fragment>
  );
};

export default Procesando;
