export default {
  apiEndpoint: "http://localhost:8085/evaCore/api/v1",
  apiSetearEstadoKiosko: "/Eva/setearEstadoKiosko",
  apiOperatividadKiosko: "/Eva/obtenerOperatividadKiosko",
};
